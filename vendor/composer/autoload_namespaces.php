<?php

// autoload_namespaces.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'Zend_' => array($vendorDir . '/zendframework/zendframework1/library'),
    'Twitter_' => array($vendorDir . '/komola/bootstrap-zend-framework/library'),
    'OSS_SNMP' => array($vendorDir . '/opensolutions/oss-snmp/src'),
    'OSS_' => array($vendorDir . '/opensolutions/oss-framework/src'),
    'IXP_' => array($baseDir . '/library'),
    'Doctrine\\ORM' => array($vendorDir . '/doctrine/orm/lib'),
    'Doctrine\\DBAL' => array($vendorDir . '/doctrine/dbal/lib'),
    'Doctrine\\Common\\Lexer\\' => array($vendorDir . '/doctrine/lexer/lib'),
    'Doctrine\\Common\\Inflector\\' => array($vendorDir . '/doctrine/inflector/lib'),
    'Doctrine\\Common\\Collections\\' => array($vendorDir . '/doctrine/collections/lib'),
    'Doctrine\\Common\\' => array($vendorDir . '/doctrine/common/lib'),
);
